import os
from nullspace_optimizer.nullspace import display,colored
examples_folder = os.path.split(__file__)[0]
display("Testing all examples in "+examples_folder,color="magenta",attr="bold")
print("")
files = os.listdir(examples_folder)
files.sort()
for f in files:
    if f.endswith('.py') and f != "test_all.py" and f != "draw.py":
        module = os.path.splitext(f)[0]
        display("="*80,color="magenta",attr="bold")
        display("Testing "+f,color="magenta",attr="bold")
        display("="*80,color="magenta",attr="bold")
        print("")
        try:
            exec("import nullspace_optimizer.examples."+module)
        except Exception as e:
            print(e)
            display("Warning, testing of "+f+" failed.",color="red",attr="bold")
        input(colored(f"\nEnd of test {f}. Press any key to continue.",color="magenta",attr="bold"))
        display("\n\n")

